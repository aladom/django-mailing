��    R      �  m   <      �     �     �     
       !   '     I     O  (   V  �         7     X  	   `     j  '   v  L   �     �  !   �     	     5	     ;	  
   C	     N	     S	     X	     ]	     l	     s	     �	  B   �	     �	     �	  .   
  J   3
  
   ~
     �
     �
     �
     �
  
   �
     �
     �
     �
                    ,     =     L     Y     a     i     v     �     �     �     �     �     �     �     �  	   �     �     �     �     �     �               %     8     ?  
   G     R     h     u     �     �     �  	   �     �     �  D  �          "     9     U  2   ]  	   �     �  -   �  �   �  .   �     �  
   �     �  .   �  T        o     w  (   �     �  
   �     �     �     �     �     �               +  K   C     �     �  !   �  E   �          +     ;     P     g  
   p     {     �     �     �     �     �     �     �                     (     ;     N     a     i     x          �     �     �  	   �     �     �     �     �     �  
   �     �          /     6     <     D  
   X     c     u     �     �     �     �     �     	       B      P   H             F   +      1   E       2   @   (   Q   <                     ;   G   J       L      A   :       ?   .   7           K   9         
   M          8   D   3   N                             '   >          /            *      0         ,              O   $         #          C   4           %                    &          R      "   6       !   -       )   5   =       I    Blocked Campaign e-mails Campaign properties Canceled Disable selected e-mail campaigns Draft E-mail E-mail subscriptions saved successfully. E-mails won't be sent for campaigns that are not enabled. Even if a script requests for sending. This is a way to turn off some campaigns temporarily without changing the source code. Enable selected e-mail campaigns Failure HTML body Hard bounce Leave blank to generate from HTML body. Leave blank to use mailing/{key}.html from within your template directories. Mailing Manage your mailing subscriptions May contain template variables. Other Pending Properties SPAM Save Sent Set debug mode Status Subscription management Subscriptions management The key will be used to reference this campaign from your scripts. Unset debug mode Unsubscribe Wheter to prefix the subject with "{}" or not. Whether potential recipients are subscribed or not to this type by default attachment attachments blacklisted address blacklisted addresses campaign debug mode description dynamic attachment dynamic attachments e-mail e-mail address e-mail campaign e-mail campaigns e-mail subject e-mail title e-mails enabled extra header extra headers failure reason file filename header headers key mail header mail headers mime type name prefix subject reason reported on scheduled on sent on static attachment static attachments status subject subscribed subscribed by default subscription subscription type subscription types subscriptions template file text body value verbose reason Project-Id-Version: django-mailing 0.1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2016-02-01 14:26+0100
Last-Translator: Etienne TACK <etienne.tack@aladom.fr>
Language-Team: French
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Bloqué E-mails de la campagne Propriétés de la campagne Annulé Désactiver les campagnes d'e-mail sélectionnées Brouillon E-mail Abonnements e-mail enregistrés avec succès. Les e-mails ne seront pas envoyés pour les campagnes qui ne sont pas activées. Même si un script demande l'envoi. Cela sert à désactiver temporairement certaines campagnes sans modifier le code source. Activer les campagnes d'e-mail sélectionnées Échec corps HTML Hard bounce Laisser vide pour générer à partir du HTML. Laisser vide pour utiliser mailing/{key}.html depuis votre répertoire de templates. Mailing Gérez vous abonnements e-mail Peut contenir des variables de template. Autre En attente Propriétés SPAM Enregistrer Envoyé Activer le mode debug Statut Gestion des abonnements Gestion des abonnements La clé sera utilisée pour référencer cette campagne depuis vos scripts. Désactiver le mode debug Désabonner Prefixer l'objet par "{}" ou non. Si les potentiels destinataires sont abonnées à ce type par défaut pièce jointe pièces jointes adresse blacklistée adresses blacklistées campagne mode debug description pièce jointe dynamique pièces jointes dynamiques e-mail adresse e-mail campagne d'e-mail campagnes d'e-mail objet de l'e-mail titre de l'email e-mails activé header additionnel header additionnel raison de l'échec fichier nom du fichier header headers clé header de l'e-mail headers de l'e-mail type MIME nom préfixer l'objet raison signalé le planifié le envoyé le pièce jointe statique pièces jointes statiques statut objet abonné abonné par défaut abonnement type d'abonnement types d'abonnement abonnements fichier template corps textuel valeur raison détaillée 